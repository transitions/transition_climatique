
.. _intro_reseaux_sociaux:

=======================
Intro Réseaux sociaux
=======================

.. seealso::

   - https://bonpote.com/les-meilleures-sources-sur-lenvironnement-et-le-climat-tous-niveaux-confondus/


.. contents::
   :depth: 3


Les comptes twitter (et autres réseaux sociaux) à suivre
=============================================================

Les réseaux sociaux peuvent être un calvaire, mais aussi très utiles
lorsque l’on s’en sert à bon escient.

Il y a quelques mois encore, je détestais Twitter, mais j’ai commencé à
suivre quelques personnes et cela m’a clairement aidé à accélérer ma
compréhension sur beaucoup de sujets transverses.

La liste des personnes comptes à suivre ci-dessous n’est pas exhaustive :
pour plus de suggestions, vous pouvez également regarder les comptes
que je suis sur Twitter (je ne suis quasiment exclusivement que des gens
intéressants… et 2-3 membres du gouvernement).

- Valérie Masson-Delmotte, Vice présidente du GIEC et certainement parmi
  les personnes les plus compétentes au monde sur son sujet
- Christophe Cassou, Directeur de Recherche au CNRS, Climatologue et
  lead author du prochain rapport du GIEC (AR6)
- Julia Steinberger, lead author du GIEC et (entre autres) autrice du
  papier des discourses of climate delay
- Jason Hickel : auteur de The Divide et Less is More
- Sylvestre Huet : meilleur journaliste scientifique français ?
- Laydgeur : vulgarisateur des questions climatiques, il propose un
  #quizzenergieclimat tous les soirs !
- Tristan Kamin : ingénieur en sûreté nucléaire, ses posts sur le nucléaire
  sont très instructifs
- Matthieu Auzanneau, auteur de l’Or noir (must read)
- Nicolas Goldberg, Consultant dans le domaine de l’énergie et militant
  insatiable pour le climat !
- Cet imposteur, qui raconte n’importe quoi 98% du temps.

Sur Linkedin
==============

- Juliette Nouel : incontournable sur Linkedin, à suivre absolument
- L’indispensable Jean Marc Jancovici, pourfendeur du greenwashing !
- Maxence Cordiez et ses posts toujours très intéressants sur l’énergie et le climat

Les inclassables
====================

Il existe quelques sites et/ou personnes qui font également de très
bons travaux mais plus difficiles à classer dans une catégorie.

J’aurais pu y glisser mes favoris internet, mais je crois qu’avec tous
les liens de l’article, il y a déjà plusieurs centaines d’heures de contenu !

- L’agence de la Transition Ecologique (ADEME) : Site de référence français,
  notamment pour tout ce qui concerne les ACV
- Carbone 4 et ses études toujours très instructives
- B&L Evolution et son étude sur le secteur aéronautique
- Un débat intéressant entre les équipes de Negawatt et du Shift Project
- 2 MOOCs : un sur les causes et enjeux du changement climatique et un
  deuxième sur la biodiversité
- Timothée Parrique et ses travaux sur la Décroissance
- Notre interview avec Olivier Fontan, directeur exécutif du HCC

