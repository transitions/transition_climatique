# Configuration file for the Sphinx documentation builder.
#
# http://www.sphinx-doc.org/en/master/config
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

project = "Climat"
author = "Earth"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
copyright = f"2018-{now.year}, {author}. Built with Python {platform.python_version()}"
source_suffix = {
     ".rst": "restructuredtext",
     ".md": "markdown",
}
extensions = ["recommonmark"]
master_doc = "index"
language = None
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv"]
extensions += ["sphinx.ext.intersphinx"]
intersphinx_mapping = {
    "documentation": ("https://gdevops.gitlab.io/tuto_documentation/", None),
}
extensions += ["sphinx.ext.todo"]
todo_include_todos = True
xtensions = [
    # https://ablog.readthedocs.io/manual/markdown/
    "myst_nb",
]
# MyST config
myst_update_mathjax = False
myst_admonition_enable = True
myst_deflist_enable = True
extensions += [
    "ablog",
]
# https://ablog.readthedocs.io/manual/ablog-configuration-options/
#####################################################################
blog_path = "transition_climatique"
# Base URL for the website, required for generating feeds.
blog_baseurl = "https://gdevops.gitlab.io/transition_climatique/"
blog_title = "transition_climatique"
# Post related
# Date display format (default is '%b %d, %Y') for published posts
post_date_format = "%Y-%m-%d"
# Number of seconds (default is 5) that a redirect page waits before
# refreshing the page to redirect to the post
post_redirect_refresh = 1
# Index of the image that will be displayed in the excerpt of the post.
# Default is 0, meaning no image.
# Setting this to 1 will include the first image, when available, to the excerpt.
# This option can be set on a per post basis using post directive option image
post_auto_image = 1
# Number of paragraphs (default is 1) that will be displayed as an excerpt from the post
post_auto_excerpt = 4
# Blog feeds
blog_feed_archives = True
blog_feed_fulltext = True
blog_feed_subtitle = False
blog_feed_titles = False
# Specify number of recent posts to include in feeds, default is None for all posts
blog_feed_length = None
# Font awesome
# ABlog templates will use of Font Awesome icons if one of the following is set: fontawesome_link_cdn
fontawesome_included = True
# https://ablog.readthedocs.io/manual/posting-and-listing/?highlight=blog_post_pattern#posting-with-page-front-matter
# Instead of adding blogpost: true to each page, you may also provide a
# pattern (or list of patterns) in your conf.py file using the blog_post_pattern option
blog_post_pattern = "actions/*/*"
html_extra_path = ["feed.xml"]
liste_full = [
    "postcard.html",
    "recentposts.html",
    "sourcelink.html",
    "archives.html",
    "tagcloud.html",
    "categories.html",
    "searchbox.html",
]
html_sidebars = {
    "index": liste_full,
    "index/**": liste_full,
    "meta/**": liste_full,
    "decouverte/**": liste_full,
    "pre_requis/**": liste_full,
    "niveau2/**": liste_full,
    "associations/**": liste_full,
    "reseaux_sociaux/**": liste_full,
}
extensions += [
    "sphinx_panels",
]
# Panels config
panels_add_bootstrap_css = False
pygments_style = "sphinx"
html_theme = "pydata_sphinx_theme"
html_theme_options = {
    "search_bar_text": "Search this site...",
    "search_bar_position": "navbar",
}


def setup(app):
    app.add_css_file("custom.css")


# https://www.sphinx-doc.org/en/master/usage/configuration.html
show_authors = True
html_show_sourcelink = True
html_copy_source = True
