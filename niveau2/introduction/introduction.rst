
.. _introduction_niveau2:

=======================
Introduction niveau2
=======================


.. seealso::

   - https://bonpote.com/les-meilleures-sources-sur-lenvironnement-et-le-climat-tous-niveaux-confondus/



.. contents::
   :depth: 3

Niveau 2
==========

.. seealso::

   - https://www.ipcc.ch/languages-2/francais/
   - https://bonpote.com/climat-peut-on-vraiment-faire-confiance-au-giec/
   - https://climatoscope.ca/
   - https://climate.nasa.gov/
   - https://ourworldindata.org/
   - https://climate.mit.edu/
   - https://skepticalscience.com/
   - https://twitter.com/skepticscience
   - https://climatetippingpoints.info/
   - https://twitter.com/climatetippoint
   - https://climatefeedback.org/
   - https://twitter.com/ClimateFdbk
   - https://www.senat.fr/espace_presse/actualites/201905/adaptation_de_la_france_aux_changements_climatiques_a_lhorizon_2050.html

Une fois les bases maîtrisées, il vous sera plus facile d’aborder le
contenu des rapports/sites/vidéos qui suivent.

Il n’y a pas vraiment d’intérêt à lire directement les contenus suivants
si vous n’avez pas les pré-requis (qui demandent déjà des dizaines
d’heures d’apprentissage).

- `Le GIEC <https://www.ipcc.ch/languages-2/francais/>`_ (IPCC) : Il est important de faire la distinction entre :

1) Les rapports d’évaluation (AR, Assessment Report, publiés tous les 4/6 ans
   et faisant une synthèse globale des connaissances sur le changement climatique)
   et les rapports spéciaux (SR, Special Report, plus courts et dédiés
   à des questions précises)
2) Les différentes parties : rapports complets (divisés en  chapitres,
   on parle d’autour de 1000 pages pour un rapport spécial ou pour un
   rapport d’un des trois groupes de travail qui forment un rapport
   d’évaluation)
   / résumés techniques des travaux de groupes (TS, Technical Summary)
   / rapport de synthèse (SYR, Synthesis Report qui résume le travail
   des trois groupes dans le cadre d’un rapport d’évaluation) / résumés
   pour les décideurs (SPM, Summary for Policymakers, un par rapport
   ainsi que par rapport de groupe de travail)

Si bien sûr vous vous demandiez si le GIEC est une source fiable, je vous
invite à lire cet article : `Climat, peut-on faire confiance au GIEC <https://bonpote.com/climat-peut-on-vraiment-faire-confiance-au-giec/>`_

`Google Scholar <https://scholar.google.com/>`_ : permet de suivre les publications de certains auteurs
et d’être informé.e lors de la publication d’un nouvel article.

- `Climatoscope <https://climatoscope.ca/>`_ : le Climatoscope est une revue francophone de vulgarisation
  scientifique portant sur les changements climatiques, publiée annuellement
  et s’adressant à un lectorat averti.
- `Carbon Brief <https://www.carbonbrief.org/>` : indispensable pour se tenir à jour des dernières
  nouvelles sur le climat
- `Le site de la NASA <https://climate.nasa.gov/>`_, avec de superbes infographies
- `Our world in Data <https://ourworldindata.org/>`_ : site très utile lorsque vous cherchez un chiffre
  sur le climat !
- `Le site du MIT sur le climat <https://climate.mit.edu/>`_
- `Skeptical Science.com <https://skepticalscience.com/>`_ : si vous voulez gagner du temps face aux climatosceptiques…
- `climatetippingpoint.info <https://climatetippingpoints.info/>`_ : tout savoir sur les points de bascule climatiques
- `Climate Feedback <https://climatefeedback.org/>`_ : site de fackchecking de news, très utile pour repérer
  certaines grosses bêtises relayées dans la presse !
- `Le rapport sénatorial « Adapter la France aux dérèglements climatiques
  à l’horizon 2050 : urgence déclarée » <https://www.senat.fr/espace_presse/actualites/201905/adaptation_de_la_france_aux_changements_climatiques_a_lhorizon_2050.html>`_:
  **ce rapport est méconnu du grand public et pourtant indispensable**.
  C’est notamment grâce à ce rapport que j’ai répondu à `l’excuse numéro 5
  de l’inaction climatique : le Free Rider <https://bonpote.com/climat-les-12-excuses-de-linaction-et-comment-y-repondre/>`_.

Extractivisme / Matières premières
==========================================

.. seealso::

   - https://journals.openedition.org/vertigo/17522
   - https://www.resourcepanel.org/
   - https://www.iea.org/


Extractivisme / Matières premières:

- `Exploitation des ressources naturelles et échange écologique inégal <https://journals.openedition.org/vertigo/17522>`_ :
  une approche globale de la dette écologique
- `Le rapport complet du Resource Panel <https://www.resourcepanel.org/>`_
- `L’agence internationale de l’énergie (IAE) <https://www.iea.org/>`_, qui sert de base pour de
  nombreux analystes du secteur de l’énergie.

PS : je recommande de vous servir des chiffres et d’en tirer vos propres
interprétations.

Par exemple, l’IAE nous promet une belle croissance verte avec un joli
découplage mondial.

Maintenant que vous avez les bases avec tous les liens précédents, vous
pourrez tirer votre propre conclusion de la faisabilité de cette croissance verte.

