.. index::
   pair: pré-requis ; JM. Jancovici
   ! JM. Jancovici
   pair: Rodolphe Meyer; pré-requis
   ! Rodolphe Meyer

.. _pre_requis_intro:

=======================
Introduction pré-requis
=======================

.. seealso::

   - https://bonpote.com/les-meilleures-sources-sur-lenvironnement-et-le-climat-tous-niveaux-confondus/

.. contents::
   :depth: 3


Introduction
===============

.. seealso::

   - https://jancovici.com/
   - https://www.youtube.com/watch?v=xgy0rW0oaFI&t=1s&ab_channel=Jean-MarcJancovici
   - https://www.youtube.com/watch?v=eruVCRrIkCY&ab_channel=LeR%C3%A9veilleur

Avant de passer au niveau 2, voici 2 séries de vidéos/cours en ligne
que j’estime **d’utilité publique**.

Je dis cela rarement, mais c’est le cas.

- `Les cours des mines de JM. Jancovici <https://www.youtube.com/watch?v=xgy0rW0oaFI&t=1s&ab_channel=Jean-MarcJancovici>`_
  sont une mine d’or : 20h de cours qui vous permettront d’aborder le
  niveau 2 avec confiance (et de **prendre quelques claques au passage**).
  `Son site <https://jancovici.com/>`_ est également une **très bonne source d’informations**.
- `Les vidéos du Réveilleur (Rodolphe Meyer) <https://www.youtube.com/watch?v=eruVCRrIkCY&ab_channel=LeR%C3%A9veilleur>`_
  Travail de vulgarisation qui n’a pas d’égal sur Youtube.
