
.. _transition_climatique_doc_meta_infos:

=====================
Meta doc
=====================

.. seealso::

   - https://framagit.org/transitions/transition_climatique

.. contents::
   :depth: 3


Head project : tuto_devops
=============================

.. seealso::

   - https://gdevops.gitlab.io/tuto_devops/


Inspiration : sphinx-blogging de Chris Holdgraf
========================================================

.. seealso::

   - https://predictablynoisy.com/posts/2020/sphinx-blogging/
   - https://gdevops.gitlab.io/tuto_ablog/posts/2020/10/10/sphinx_blogging.html
   - https://gdevops.gitlab.io/tuto_ablog/
   - https://gdevops.gitlab.io/tuto_documentation/



Gitlab project
=================

.. seealso::

   - https://framagit.org/transitions/transition_climatique


Issues
---------

.. seealso::

   - https://framagit.org/transitions/transition_climatique/-/boards


Pipelines
--------------

.. seealso::

   - https://framagit.org/transitions/transition_climatique/-/pipelines


root directory
===============

::

    $ ls -als

::

    $ ls -als
    total 148
    4 drwxr-xr-x 6 pvergain pvergain  4096 nov.  13 08:00 .
    4 drwxr-xr-x 3 pvergain pvergain  4096 nov.  13 07:39 ..
    4 drwxr-xr-x 4 pvergain pvergain  4096 nov.  13 07:47 _build
    4 -rw-r--r-- 1 pvergain pvergain  3501 nov.  13 07:49 conf.py
    4 -rw-r--r-- 1 pvergain pvergain   112 nov.  13 08:00 feed.xml
    4 drwxr-xr-x 8 pvergain pvergain  4096 nov.  13 07:54 .git
    4 -rw-r--r-- 1 pvergain pvergain   118 mars   3  2020 .gitignore
    4 -rw-r--r-- 1 pvergain pvergain   250 oct.   7 10:36 .gitlab-ci.yml
    4 drwxr-xr-x 2 pvergain pvergain  4096 oct.  19 08:05 index
    4 -rw-r--r-- 1 pvergain pvergain   288 nov.  13 07:52 index.rst
    4 -rw-r--r-- 1 pvergain pvergain  1153 nov.  11 18:21 Makefile
    4 drwxr-xr-x 3 pvergain pvergain  4096 nov.  13 07:53 meta
    88 -rw-r--r-- 1 pvergain pvergain 89593 nov.  13 07:50 poetry.lock
    4 -rw-rw-rw- 1 pvergain pvergain  1142 nov.  13 07:41 .pre-commit-config.yaml
    4 -rw-r--r-- 1 pvergain pvergain   522 nov.  13 07:50 pyproject.toml
    4 -rw-rw-rw- 1 pvergain pvergain  2172 nov.  13 07:50 requirements.txt




pyproject.toml
=================

.. literalinclude:: ../../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../../Makefile
   :linenos:
