.. index::
   ! Découverte

.. _intro_decouverte:

=======================
Introduction découverte
=======================

.. seealso::

   - https://bonpote.com/les-meilleures-sources-sur-lenvironnement-et-le-climat-tous-niveaux-confondus/


.. contents::
   :depth: 3

Introduction
==============

Lorsque l’on commence à s’intéresser à un sujet, il n’est jamais facile
de savoir par où commencer.

L’objectif de cet article est de faciliter les recherches aux lecteur.ices
et également de gagner du temps dans la compréhension de l’enjeu du siècle : la sauvegarde de l’environnement.

S’intéresser à l’environnement et au climat, c’est s’intéresser à absolument
tous les sujets : les sciences naturelles, l’énergie, la politique,
la philosophie, la psychologie, la sociologie, l’économie, la justice climatique,
l’histoire des techniques…

La mauvaise nouvelle, c’est que vous ne pourrez jamais dire de votre
vivant que vous maîtrisez complètement tous les sujets.

La bonne, c’est que grâce aux liens ci-dessous, vous ne pourrez jamais
vous ennuyer !

Bien sûr, la catégorisation des liens est subjective et peut être amenée
à évoluer.

Chacun.e s’est intéressé.e à ces sujets d’une certaine façon et les
leviers ne sont pas les mêmes selon les personnes.
Si j’avais la solution miracle avec une vidéo de 5 min expliquant tout
le problème et comment le régler, nous n’en serions pas là.

Cela demande du temps (20H+ pour une compréhension large du problème
et un réveil de conscience) ! Mais l’important, c’est bel et bien de commencer.

Exceptionnellement, cet article pourra être mis à jour et complété au
fil du temps et avec les différents retours utiles des lecteur.ices.

L’ordre n’est pas vraiment volontaire par chapitre, en aucun cas je ne
me permettrais d’émettre un jugement de valeur sur ces travaux.


Niveau 1 : découverte
=======================

.. seealso::

   - https://www.conventioncitoyennepourleclimat.fr
   - https://www.hautconseilclimat.fr
   - https://www.ecologie.gouv.fr/politiques/climat
   - https://un-denial.com/2019/02/02/by-nate-hagens-reality-101-what-every-student-and-citizen-should-know-about-energy
   - https://www.encyclopedie-environnement.org/rubrique/climat
   - https://www.climat-en-questions.fr
   - https://meteofrance.com/changement-climatique


Voici quelques liens qui permettent d’attaquer le sujet.

Si vous maîtrisez l’ensemble des sources ci-dessous : FELICITATIONS !
Vous ferez partie des 1% des français dans ce cas (avec un objectif utopique de…10% ?).

- `Le « socle d’information initial » de la Convention Citoyenne pour le climat <https://www.conventioncitoyennepourleclimat.fr/wp-content/uploads/2019/10/03102019-convcit-socledoc-web.pdf>`_.
  C’est une excellente synthèse du problème climatique
- `Une vidéo très bien faite de 5 min <https://www.youtube.com/watch?v=tk-QuMCbw2I&feature=youtu.be&ab_channel=CitoyenspourleClimat>`_ sur le rapport du GIEC sur le Climat
  décrypté par `Citoyens pour le Climat <https://twitter.com/CPLCFrance>`_
- `Le site du ministère de la transition écologique <https://www.ecologie.gouv.fr/politiques/climat>`_, notamment pour comprendre
  les politiques publiques, comme par ex la SNBC (Stratégie Nationale Bas Carbone).
- `Le Rapport Grand Public du HCC <https://www.hautconseilclimat.fr/publications/rapport-2019-grand-public/>`_ (Haut Conseil pour le Climat) : encore
  un très bon travail du HCC avec ce résumé de 12 pages qui se lit facilement.
- `IPBES : résumé du rapport de l’IPBES <https://drive.google.com/file/d/15maVGWyQTKCtUjBgnJ92zfMu5YIzqu_v/view>`_
  (Plateforme Intergouvernementale Scientifique et Politique sur la Biodiversité et les Services Écosystémiques
  (IPBES en anglais). **Rappel important : l’environnement, ce n’est pas
  qu’une histoire de CO2 !**
- Pour les anglophones, `la série de 4 vidéos de Nate Hagens Reality 101 :
  What every student (and citizen) should know <https://un-denial.com/2019/02/02/by-nate-hagens-reality-101-what-every-student-and-citizen-should-know-about-energy/>`_

3 sites si vous souhaitez parfaire vos connaissances sur le climat :

-  `Encyclopédie -  environnement <https://www.encyclopedie-environnement.org/rubrique/climat/>`_ par https://twitter.com/encyc_environm
-  `Climat en question <https://www.climat-en-questions.fr/>`_
-  `Météo France sur la partie changement climatique <https://meteofrance.com/changement-climatique>`_

Enfin, IMPOSSIBLE de comprendre le monde d’aujourd’hui sans une approche systémique.
Impossible de comprendre les problématiques climatiques sans cela.

Après avoir échangé avec des étudiants et professeurs d’autres pays,
**il y a en France un vrai déficit à ce niveau là.**.

J’espère donc que ces 2 courtes vidéos vous seront utiles :

-  `La pensée systémique (eng) <https://www.youtube.com/watch?v=-sfiReUu3o0&>`_
-  `Boucles de rétroaction et les rythmes de la nature <https://www.youtube.com/watch?v=inVZoI1AkC8>`_

