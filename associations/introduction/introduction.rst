
.. _intro_associations_climat:

=======================
Intro Associations
=======================

.. seealso::

   - https://bonpote.com/les-meilleures-sources-sur-lenvironnement-et-le-climat-tous-niveaux-confondus/


.. contents::
   :depth: 3

Associations / Engagement
===============================

.. seealso::

   - https://fresqueduclimat.org/
   - https://avenirclimatique.org/
   - https://twitter.com/avenirclim
   - https://theshiftproject.org/
   - https://notreaffaireatous.org/
   - https://pour-un-reveil-ecologique.org/fr/
   - https://notreaffaireatous.org/impacts-la-revue-de-presse-des-inegalites-climatiques/
   - https://bonpote.com/comment-calculer-son-empreinte-carbone/

Une fois arrivé.es à la 5ème étape du deuil, vous aurez compris que
la solution, c’est de passer à l’action !

Voici une liste (non exhaustive) d’associations/think tank qui œuvrent
au quotidien pour **alerter sur le changement climatique**

- `La fresque du climat <https://fresqueduclimat.org/>`_ : apprendre sur le climat de façon ludique, je
  recommande vivement !
- `Avenir climatique <https://avenirclimatique.org/>`_ : du contenu et des MOOCs de qualité
- `Shift Project <https://theshiftproject.org/>`_ : le think tank de Jean Marc Jancovici avec une équipe
  de bénévoles très motivé.es
- `Pour un réveil écologique <https://pour-un-reveil-ecologique.org/fr/>`_ : Si vous êtes étudiant ou jeune diplôme,
  il faut les rejoindre !
- `Notre affaire à tous <https://notreaffaireatous.org/>`_ : association dédiée à la justice climatique.
  Je vous recommande de vous abonner à `la Newsletter IMPACT sur les
  inégalités climatiques <https://notreaffaireatous.org/impacts-la-revue-de-presse-des-inegalites-climatiques>`_.

Enfin, `pensez à calculer votre empreinte carbone (en 5 min) via ce
simulateur d’Ecolab <https://bonpote.com/comment-calculer-son-empreinte-carbone/>`_ : c’est ce qui a (et de loin) le mieux marché pour
éveiller les consciences !
